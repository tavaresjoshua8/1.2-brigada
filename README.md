## Folder Structure

The workspace contains folder:

- `src`: the folder to maintain sources

Meanwhile, the compiled output files will be generated in the `bin` folder by default.
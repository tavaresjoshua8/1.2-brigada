package util;

/*
    * Brigada de Rescate - Implementando Collections
    * Practica Numero: 1.2-b
    * Autor: Pedro Joshua Salcido Tavares
*/
public class Spacing {
    public static final String ACCIDENTE_TITLE = String.format("%4s %4s %5s | %-9s | %-8s\n", "", "Tipo", "", "Prioridad", "Personas");
    
    public static String accidente(Accidente accidente) {
        return String.format("%-15s | %3s %1d %3s | %d", accidente.getTipo(), "", accidente.getPrioridad(), "", accidente.getPersonasAtender());
    }
}

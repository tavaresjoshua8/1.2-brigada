package util;

import java.util.Comparator;

/*
    * Brigada de Rescate - Implementando Collections
    * Practica Numero: 1.2-b
    * Autor: Pedro Joshua Salcido Tavares
*/
public class Accidente {
    private String tipo = "No definido";
    private int prioridad = 0; // Priodirad de 1 a 9
    private int personasAtender = 0;

    public Accidente(String tipo, int prioridad, int personasAtender) {
        this.setTipo(tipo);
        this.setPrioridad(prioridad);
        this.setPersonasAtender(personasAtender);
    }

    public void atender() {
        if(personasAtender <= 0)
            return;

        System.out.println("Atendiendo accidente " + this.tipo);
        this.personasAtender--;
    }

    @Override
    public String toString() {
        return Spacing.accidente(this);
    }

    // Getters
    public String getTipo() { return this.tipo; }
    public int getPrioridad() { return this.prioridad; }
    public int getPersonasAtender() { return this.personasAtender; }

    // Setters
    public void setTipo(String tipo) { this.tipo = tipo; }
    public void setPrioridad(int prioridad) { 
        this.prioridad = (prioridad < 10) ? prioridad : 9;
    }
    public void setPersonasAtender(int personasAtender) { this.personasAtender = personasAtender; }

    // Comparators
    public static Comparator<Accidente> prioridadComparator = new Comparator<Accidente>() {
        // Accidentes con menor prioridad primero
        @Override
        public int compare(Accidente a1, Accidente a2) {
            if(a1.getPrioridad() == a2.getPrioridad())
                return a1.getPersonasAtender() - a2.getPersonasAtender();

            return a1.getPrioridad() - a2.getPrioridad();
        }
    };
}

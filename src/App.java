import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import util.Accidente;
import util.Spacing;

/*
    * Brigada de Rescate - Implementando Collections
    * Practica Numero: 1.2-b
    * Autor: Pedro Joshua Salcido Tavares
*/
public class App {
    private static final ArrayList<Accidente> accidentes = new ArrayList<Accidente>();
    private static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        cargarAccidentes();

        mostrarAccidentes();

        int opcion;
        do {
            System.out.println();
            opcion = menu();
            switch(opcion) {
                case 1:
                    accidentes.add(nuevoAccidente());
                    break;
                case 2:
                    Accidente mayorPrioridad = Collections.max(accidentes, Accidente.prioridadComparator);
                    if(mayorPrioridad.getPersonasAtender() <= 0) 
                        System.out.println("No hay accidentes que atender");
                    mayorPrioridad.atender();
                    break;
                case 3:
                    int personasTotales = accidentes.stream().mapToInt(mapper -> mapper.getPersonasAtender()).sum();
                    System.out.println("Personas totales: " + personasTotales);
                    break;
                case 4:
                    Accidente menorPrioridad = Collections.min(accidentes, Accidente.prioridadComparator);
                    System.out.println("Menor prioridad: " + menorPrioridad.getPrioridad());
                    System.out.println("Tipo: " + menorPrioridad.getTipo());
                    System.out.println("Personas atender: " + menorPrioridad.getPersonasAtender());
                    break;
                case 5:
                    System.out.print(Spacing.ACCIDENTE_TITLE);
                    accidentes.stream().filter((accidente) -> accidente.getPersonasAtender() > 0).forEach(System.out::println);
                    break;
                case 6:
                    mostrarAccidentes();
                    break;
            }
        } while(opcion != 0);
    }

    private static Accidente nuevoAccidente() {
        input.nextLine();
        System.out.print("Ingrese el tipo: \n> ");
        String tipo = input.nextLine();

        int prioridad = leerEntero("Ingrese la prioridad: ");
        int cantidadPersonas = leerEntero("Ingrese la cantidad de personas: ");

        return new Accidente(tipo, prioridad, cantidadPersonas);
    }

    private static int menu() {
        System.out.println("==============================");
        System.out.println("1. Nuevo Accidente");
        System.out.println("2. Atender Accidente");
        System.out.println("3. Cantidad de personas a rescatar");
        System.out.println("4. Accidente con menor prioridad");
        System.out.println("5. Mostrar accidentes a rescatar");
        System.out.println("6. Mostrar accidentes");
        System.out.println("0. Salir");
        System.out.println("==============================");

        return leerEntero("Ingrese una opcion:");
    }

    private static void cargarAccidentes() {
        System.out.println("Creando accidentes...");
        System.out.println("Terremoto...");
        accidentes.add(new Accidente("Terremoto", 5, leerEntero("Cantidad de personas a atender")));

        System.out.println("\nIncendio...");
        accidentes.add(new Accidente("Incendio", 4, leerEntero("Cantidad de personas a atender")));

        System.out.println("\nInundacion...");
        accidentes.add(new Accidente("Inundacion", 3, leerEntero("Cantidad de personas a atender")));

        System.out.println("\nAutomovilistico...");
        accidentes.add(new Accidente("Automovilistico", 2, leerEntero("Cantidad de personas a atender")));
    }

    public static void mostrarAccidentes() {
        System.out.println("\nAccidentes Creados:");
        System.out.print(Spacing.ACCIDENTE_TITLE);
        for (Accidente accidente : accidentes) {
            System.out.println(accidente);
        }
    }

    private static int leerEntero(String mensaje) {
        try {
            System.out.print(mensaje + "\n> ");
            return input.nextInt();
        } catch (Exception e) {
            input.nextLine();
            System.err.println("Error: Ingrese un entero valido.");
            return leerEntero(mensaje);
        }
    }
}
